# SpringBoot2整合quartz

#### 介绍
SpringBoot2.x整合Quartz


#### 安装教程

1. 安装jdk8、maven、git、mysql
2. 前往quartz官网下载SNAPSHOT包

#### 使用说明

1. clone项目到本地
2. 在mysql中创建数据库quartz_test
3. 在项目的src--->main--->resources--->sql找到good_info.sql，执行sql语句
4. 解压quartz的SNAPSHOT包，在src--->org--->quartz--->impl--->jdbcjobstore找到tables_mysql.sql，执行sql语句
5. 完成以上步骤后，启动idea配置maven，并导入项目
6. 启动项目后，查看GoodController以及TimerController中的方法，按照方法中的参数请求即可看到控制台输出