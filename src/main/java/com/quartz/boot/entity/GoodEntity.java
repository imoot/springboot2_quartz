package com.quartz.boot.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 15:08
 */
@Entity
@Table(name = "good_info")
@Data
public class GoodEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "good_name")
    private String name;

}
