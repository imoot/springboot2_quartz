package com.quartz.boot.controller;

import com.quartz.boot.quartz.JobOperation;
import com.quartz.boot.timer.GoodStockCheckTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 16:52
 */
@RestController
@RequestMapping("/timer")
public class TimerController {

    private final JobOperation jobOperation;

    @Autowired
    public TimerController(JobOperation jobOperation) {
        this.jobOperation = jobOperation;
    }

    @RequestMapping("/remove")
    public String remove(String name, String group) {
        try {
            jobOperation.remove(name, group);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    @RequestMapping("/add")
    public String add(String name, String group) {
        try {
            String cron = "0/30 * * * * ?";
            jobOperation.add(name, group, cron);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    @RequestMapping("/modify")
    public String modify(String name, String group) {
        try {
            String cron = "0/35 * * * * ?";
            jobOperation.modify(name, group, cron);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    @RequestMapping("/shutdown")
    public String shutdown() {
        try {
            jobOperation.removeJobs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }
}
