package com.quartz.boot.controller;

import com.quartz.boot.entity.GoodEntity;
import com.quartz.boot.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 15:13
 */
@RestController
@RequestMapping("/good")
public class GoodController {

    private final GoodService goodService;

    @Autowired
    public GoodController(GoodService goodService) {
        this.goodService = goodService;
    }

    @RequestMapping("/put")
    public int save(GoodEntity good) {
        return goodService.saveGood(good);
    }
}
