package com.quartz.boot.dao;

import com.quartz.boot.entity.GoodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 15:12
 */
@Repository
public interface GoodRepository extends JpaRepository<GoodEntity, Integer> {
}
