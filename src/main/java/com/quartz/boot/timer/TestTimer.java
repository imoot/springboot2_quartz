package com.quartz.boot.timer;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Date;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 15:20
 */
public class TestTimer extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("执行测试任务，执行时间：" + new Date());
    }
}
