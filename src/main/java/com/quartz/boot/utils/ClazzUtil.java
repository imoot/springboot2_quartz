package com.quartz.boot.utils;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 17:06
 */
public class ClazzUtil {

    public static Class getClazz(String packageName,String className){
        Class clazz;
        try {
            clazz = Class.forName(packageName+"."+className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return clazz;
    }

}
