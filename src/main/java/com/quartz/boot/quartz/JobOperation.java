package com.quartz.boot.quartz;

import com.quartz.boot.utils.ClazzUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 16:54
 */
@Component
public class JobOperation {

    private final Scheduler scheduler;

    @Autowired
    public JobOperation(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @SuppressWarnings("unchecked")
    public void add(String name, String group, String cron) throws Exception {
        Class clazz = ClazzUtil.getClazz(QuartzContants.package_url, name);
        if (null == clazz) {
            throw new ClassNotFoundException("没有这个定时任务");
        }
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cron);
        //创建任务
        JobDetail jobDetail = JobBuilder.newJob(clazz).withIdentity(name, group).build();
        //创建触发器
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(name, group).withSchedule(scheduleBuilder).build();
        //把触发器与任务绑定到调度器中
        scheduler.scheduleJob(jobDetail, trigger);
    }

    public void remove(String name, String group) throws Exception {
        TriggerKey triggerKey = TriggerKey.triggerKey(name, group);
        //停止触发器
        scheduler.pauseTrigger(triggerKey);
        //移除触发器
        scheduler.unscheduleJob(triggerKey);
        //删除任务
        scheduler.deleteJob(JobKey.jobKey(name, group));
    }

    public void modify(String name, String group, String cron) throws Exception {
        //修改因为会额外触发一次，所以先做移除再添加
        //移除
        remove(name, group);
        //添加
        add(name, group, cron);
    }

    public void removeJobs() throws Exception {
        scheduler.pauseAll();
        scheduler.clear();
    }

}
