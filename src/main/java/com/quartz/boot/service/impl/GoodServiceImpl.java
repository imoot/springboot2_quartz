package com.quartz.boot.service.impl;

import com.quartz.boot.dao.GoodRepository;
import com.quartz.boot.entity.GoodEntity;
import com.quartz.boot.quartz.JobOperation;
import com.quartz.boot.service.GoodService;
import com.quartz.boot.timer.GoodAddTimer;
import com.quartz.boot.timer.GoodStockCheckTimer;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 15:15
 */
@Service
public class GoodServiceImpl implements GoodService {

    private final GoodRepository goodRepository;
    private final Scheduler scheduler;
    private final JobOperation jobOperation;

    @Autowired
    public GoodServiceImpl(GoodRepository goodRepository, Scheduler scheduler, JobOperation jobOperation) {
        this.goodRepository = goodRepository;
        this.scheduler = scheduler;
        this.jobOperation = jobOperation;
    }

    @Override
    public int saveGood(GoodEntity good) {
        goodRepository.save(good);
        try {
            buildCreateGoodTimer();
            buildGoodStockCheckTimer();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return good.getId();
    }

    @Override
    public void buildCreateGoodTimer() throws Exception {
        //设置开始时间为1分钟后
        long startTime = System.currentTimeMillis() + 1000 * 60;
        //任务名称
        String name = UUID.randomUUID().toString();
        //任务所属组
        String group = GoodAddTimer.class.getName();
        //创建任务
        JobDetail jobDetail = JobBuilder.newJob(GoodAddTimer.class).withIdentity(name, group).build();
        //创建触发器
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(name, group).startAt(new Date(startTime)).build();
        //把触发器与任务绑定到调度器中
        scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public void buildGoodStockCheckTimer() throws Exception {
        //任务名称
        String name = GoodStockCheckTimer.class.getName();
        //任务所属组
        String group = "Goods";
        String cron = "0/30 * * * * ?";
        jobOperation.add(name, group, cron);
    }
}
