package com.quartz.boot.service;

import com.quartz.boot.entity.GoodEntity;

/**
 * @author imoot@gamil.com
 * @date 2019/7/11 0011 15:14
 */
public interface GoodService {

    int saveGood(GoodEntity good);

    void buildCreateGoodTimer() throws Exception;

    void buildGoodStockCheckTimer() throws Exception;
}
